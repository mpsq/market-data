/* eslint import/namespace: [2, { allowComputed: true }] */
import { combineEpics } from 'redux-observable'
import { Observable } from 'rxjs/Observable'
import * as actions from '../actions'
import { api } from '../services'
import { CURRENCY } from '../static'
import { TickerVO } from '../utils'

export const getTicker = action$ => action$
  .ofType(actions.GET_TICKER)
  .switchMapTo(
    api.getTicker(CURRENCY.map(curr => `5~CCCAGG~${curr}~USD`))
      .catch(details => Observable.of({ data: {}, error: true }))
      .map(raw => {
        if (raw.error) {
          return actions.receiveBCH(raw)
        }

        const data = TickerVO(raw)

        return actions[`receive${data.fromSymbol}`]({ data, error: false })
      })
  )

export const getConnectionStatus = action$ => action$
  .ofType(actions.GET_CONNECTION_STATUS)
  .switchMapTo(
    api.getConnectionStatus()
      .map(actions.receiveConnectionStatus)
  )

const fetchBtcPrices = action$ => action$
  .ofType(actions.FETCH_BTC_PRICES)
  .switchMapTo(
    api.getBtcHistoricalPrices()
      .catch(details => Observable.of({ data: [], error: true }))
      .map(data => actions.receiveBtcPrices({ data, error: false, type: actions.FETCH_BTC_PRICES }))
  )

export const rootEpic = combineEpics(
  fetchBtcPrices,
  getConnectionStatus,
  getTicker
)
