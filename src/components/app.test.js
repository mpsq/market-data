
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import { CONNECTION_STATUS, TAB } from '../static'
import { App } from './app'

Enzyme.configure({ adapter: new Adapter() })

describe('App', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<App connectionStatus={CONNECTION_STATUS.ONLINE} selected={TAB.PRICES} />)

    expect(wrapper).toMatchSnapshot()
  })

  it('should render the selected tab', () => {
    const wrapper = shallow(<App connectionStatus={CONNECTION_STATUS.ONLINE} selected={TAB.TICKER} />)
    const wrapperTable = shallow(<App connectionStatus={CONNECTION_STATUS.ONLINE} selected={TAB.PRICES} />)

    expect(wrapper.find('Connect(Ticker)')).toHaveLength(1)
    expect(wrapper.find('Connect(Table)')).toHaveLength(0)
    expect(wrapper.find('Connect(HistoricalBtcPrices)')).toHaveLength(0)

    expect(wrapperTable.find('Connect(Ticker)')).toHaveLength(0)
    expect(wrapperTable.find('Connect(Table)')).toHaveLength(1)
    expect(wrapperTable.find('Connect(HistoricalBtcPrices)')).toHaveLength(0)
  })

  it('should render the connection status"', () => {
    const wrapper = shallow(<App connectionStatus={CONNECTION_STATUS.ONLINE} selected={TAB.PRICES} />)
    const wrapperOffline = shallow(<App connectionStatus={CONNECTION_STATUS.OFFLINE} selected={TAB.PRICES} />)

    expect(wrapper.contains('Online')).toBe(true)
    expect(wrapper.contains('Offline')).toBe(false)
    expect(wrapperOffline.contains('Online')).toBe(false)
    expect(wrapperOffline.contains('Offline')).toBe(true)
  })
})
