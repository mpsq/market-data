
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import { Ticker } from './ticker'

Enzyme.configure({ adapter: new Adapter() })

const ticker = {
  bch: {
    fromSymbol: 'bch',
    priceFlag: 1
  },
  btc: {
    fromSymbol: 'btc',
    priceFlag: 2
  },
  eth: {
    fromSymbol: 'eth',
    priceFlag: 1
  },
  xrp: {
    fromSymbol: 'xrp',
    priceFlag: 1
  }
}

describe('Ticker', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Ticker className='my-class' error={false} ticker={ticker} />)

    expect(wrapper).toMatchSnapshot()
  })

  it('should show a message when there is an error"', () => {
    const wrapperError = shallow(<Ticker className='my-class' error={true} ticker={ticker} />)
    const wrapperOk = shallow(<Ticker className='my-class' error={false} ticker={ticker} />)

    expect(wrapperError.find('.error')).toHaveLength(1)
    expect(wrapperOk.find('.error')).toHaveLength(0)
  })
})
