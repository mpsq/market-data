import { distanceInWordsStrict } from 'date-fns'
import _ from 'lodash/fp'
import PropTypes from 'prop-types'
import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { getTickerData, getTickerError } from '../reducers'
import { PRICE_FLAG } from '../static'
import { FormatNumber } from '../utils'

import styles from './ticker.module.css'

const tickerRenderer = _.map(data => {
  const lastUpdate = distanceInWordsStrict(new Date(), new Date(data.lastUpdate), { addSuffix: true })
  const change = FormatNumber(data.priceDailyDiff)

  return (
    <div key={data.fromSymbol} className={styles.ticker}>
      <div className={styles.symbol}>{data.fromSymbol}{' → '}{data.toSymbol}</div>
      <div className={styles.info}>
        <span className={styles.label}>Price (USD)</span>
        <span className={`${styles[PRICE_FLAG[data.priceFlag].toLowerCase()]} ${styles.value}`}>{FormatNumber(data.price)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Change 24H</span>
        <span className={`${styles.value} ${change < 0 ? styles.changeNegative : styles.changePositive}`}>{change}%</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Last volume</span>
        <span className={styles.value}>{FormatNumber(data.lastVolume)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Last volume to</span>
        <span className={styles.value}>{FormatNumber(data.lastVolumeTo)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Volume 24H</span>
        <span className={styles.value}>{FormatNumber(data.volumeDailyHour)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Volume 24H to</span>
        <span className={styles.value}>{FormatNumber(data.volumeDailyHourTo)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Volume 1H</span>
        <span className={styles.value}>{FormatNumber(data.volumeHour)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Volume 1H to</span>
        <span className={styles.value}>{FormatNumber(data.volumeHourTo)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>High 24H</span>
        <span className={styles.value}>{FormatNumber(data.highDailyHour)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Low 24H</span>
        <span className={styles.value}>{FormatNumber(data.lowDailyHour)}</span>
      </div>
      <div className={styles.info}>
        <span className={styles.label}>Open 24H</span>
        <span className={styles.value}>{FormatNumber(data.openDailyHour)}</span>
      </div>
      { data.lastUpdate && <div className={styles.lastUpdate}>Last update {lastUpdate}</div> }
    </div>
  )
})

export class Ticker extends PureComponent {
  static propTypes = {
    className: PropTypes.string.isRequired,
    error: PropTypes.bool.isRequired,
    ticker: PropTypes.shape({
      bch: PropTypes.object,
      btc: PropTypes.object,
      eth: PropTypes.object,
      xrp: PropTypes.object
    }).isRequired
  }

  componentWillReceiveProps () {
    clearInterval(this.interval)

    // This interval makes sure we update the "last update" label
    // in case the previous update is more than 1 second ago.
    this.interval = setInterval(() => this.forceUpdate(), 1000)
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  render () {
    const { props } = this

    if (props.error) {
      return (
        <div className={`${props.className} ${styles.root} ${styles.error}`}>
            Oops, something went wrong! Please try refreshing the page :)
        </div>
      )
    }

    return (
      <div className={`${props.className} ${styles.root}`}>
        {tickerRenderer(props.ticker)}
      </div>
    )
  }
}

export default connect(
  state => ({
    error: getTickerError(state),
    ticker: getTickerData(state)
  })
)(Ticker)
