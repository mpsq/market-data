
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import { Table } from './table'

Enzyme.configure({ adapter: new Adapter() })

const ticker = {
  bch: {},
  btc: {},
  eth: {},
  xrp: {}
}

describe('Table', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Table className='my-class' error={false} ticker={ticker} />)

    expect(wrapper).toMatchSnapshot()
  })

  it('should show a message when there is an error"', () => {
    const wrapperError = shallow(<Table className='my-class' error={true} ticker={ticker} />)
    const wrapperOk = shallow(<Table className='my-class' error={false} ticker={ticker} />)

    expect(wrapperError.find('.error')).toHaveLength(1)
    expect(wrapperOk.find('.error')).toHaveLength(0)
  })
})
