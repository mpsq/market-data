import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { selectTab } from '../actions'
import { getSelectedTab } from '../reducers'
import { TAB } from '../static'

import styles from './summary.module.css'

export class Summary extends Component {
  static propTypes = {
    selected: PropTypes.string.isRequired,
    selectTab: PropTypes.func.isRequired
  }

  onClickTicker = () => {
    this.props.selectTab(TAB.TICKER)
  }

  onClickPrices = () => {
    this.props.selectTab(TAB.PRICES)
  }

  onClickTrades = () => {
    this.props.selectTab(TAB.HISTORICAL)
  }

  render () {
    const { onClickTicker, onClickPrices, onClickTrades, props } = this
    return (
      <div className={styles.root}>
        <div className={styles.title}>
            CRYPTOCURRENCY MARKET
        </div>
        <div className={styles.inner}>
          <div {...{
            className: `${styles.tab} ${props.selected === TAB.TICKER ? styles.selected : ''}`,
            onClick: onClickTicker
          }}>
            Ticker
          </div>
          <div {...{
            className: `${styles.tab} ${props.selected === TAB.PRICES ? styles.selected : ''}`,
            onClick: onClickPrices
          }}>
            Prices
          </div>
          <div {...{
            className: `${styles.tab} ${props.selected === TAB.HISTORICAL ? styles.selected : ''}`,
            onClick: onClickTrades
          }}>
            Bitcoin 1yr Price
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    selected: getSelectedTab(state)
  }),
  {
    selectTab
  }
)(Summary)
