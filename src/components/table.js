import _ from 'lodash/fp'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import ReactTable from 'react-table'
import { getTickerData, getTickerError } from '../reducers'
import { PRICE_FLAG } from '../static'
import { FormatNumber } from '../utils'

import 'react-table/react-table.css'
import styles from './table.module.css'

const ChangeCell = row => {
  const change = FormatNumber(row.value)
  return (
    <div className={`${styles.number} ${change < 0 ? styles.changeNegative : styles.changePositive}`}>{change}%</div>
  )
}

const NumberCell = row => (
  <div className={styles.number}>{FormatNumber(row.value)}</div>
)

const PriceCell = row => (
  <div className={`${styles.number} ${styles[PRICE_FLAG[row.original.priceFlag].toLowerCase()]}`}>{FormatNumber(row.value)}</div>
)

const columns = [{
  Header: 'SYMBOL',
  accessor: 'fromSymbol'
}, {
  Cell: PriceCell,
  Header: 'PRICE (USD)',
  accessor: 'price'
}, {
  Cell: ChangeCell,
  Header: 'CHANGE 24H %',
  accessor: 'priceDailyDiff'
}, {
  Cell: NumberCell,
  Header: 'LAST VOLUME',
  accessor: 'lastVolume'
}, {
  Cell: NumberCell,
  Header: 'VOLUME 24H',
  accessor: 'volumeDailyHour'
}, {
  Cell: NumberCell,
  Header: 'VOLUME 24H TO',
  accessor: 'volumeDailyHourTo'
}, {
  Cell: NumberCell,
  Header: 'HIGH 1H',
  accessor: 'highDailyHour'
}, {
  Cell: NumberCell,
  Header: 'LOW 1H',
  accessor: 'lowDailyHour'
}, {
  Cell: NumberCell,
  Header: 'OPEN 1H',
  accessor: 'openDailyHour'
}]

export const Table = props => {
  if (props.error) {
    return (
      <div className={`${props.className} ${styles.error}`}>
        Oops, something went wrong! Please try refreshing the page :)
      </div>
    )
  }

  return (
    <div className={`${props.className}`}>
      <ReactTable {...{
        columns,
        data: _.map(_.identity, props.ticker),
        filterable: true,
        showPagination: false
      }} />
    </div>
  )
}

Table.propTypes = {
  className: PropTypes.string.isRequired,
  error: PropTypes.bool.isRequired,
  ticker: PropTypes.shape({
    bch: PropTypes.object,
    btc: PropTypes.object,
    eth: PropTypes.object,
    xrp: PropTypes.object
  }).isRequired
}

export default connect(
  state => ({
    error: getTickerError(state),
    ticker: getTickerData(state)
  })
)(Table)
