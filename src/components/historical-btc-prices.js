import { scaleTime } from 'd3-scale'
import _ from 'lodash/fp'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { ChartCanvas, Chart } from 'react-stockcharts'
import { XAxis, YAxis } from 'react-stockcharts/lib/axes'
import { fitWidth } from 'react-stockcharts/lib/helper'
import { AreaSeries } from 'react-stockcharts/lib/series'
import { fetchBtcPrices } from '../actions'
import { getBtcHistoricalPricesData, getBtcHistoricalPricesError } from '../reducers'

import styles from './historical-btc-prices.module.css'

const xAccessor = data => data.x
const yAccessor = data => data.y

export class HistoricalBtcPrices extends React.Component {
  constructor (props) {
    super(props)

    props.fetchBtcPrices()
  }

  render () {
    const { props } = this

    if (props.error) {
      return (
        <div className={`${props.className} ${styles.root} ${styles.error}`}>
          Oops, something went wrong! Please try refreshing the page :)
        </div>
      )
    }

    if (_.isEmpty(props.data)) {
      return (
        <div className={`${props.className} ${styles.root} ${styles.loading}`}>
          Loading...
        </div>
      )
    }

    return (
      <div className={`${props.className} ${styles.root}`}>
        <ChartCanvas
          {...{
            data: props.data,
            height: 750,
            seriesName: 'BTC 1YR PRICE',
            type: 'svg',
            ratio: 1,
            width: props.width,
            xAccessor,
            displayXAccessor: xAccessor,
            xScale: scaleTime()
          }}>
          <Chart {...{
            id: 0,
            yExtents: yAccessor
          }}>
            <XAxis {...{
              axisAt: 'bottom',
              orient: 'bottom',
              ticks: 6
            }} />
            <YAxis {...{
              axisAt: 'left',
              orient: 'left'
            }} />
            <AreaSeries yAccessor={yAccessor}/>
          </Chart>
        </ChartCanvas>
      </div>
    )
  }
}

HistoricalBtcPrices.propTypes = {
  className: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({
    x: PropTypes.instanceOf(Date).isRequired,
    y: PropTypes.number.isRequired
  })),
  error: PropTypes.bool.isRequired,
  fetchBtcPrices: PropTypes.func.isRequired,
  width: PropTypes.number.isRequired
}

export default connect(
  state => ({
    data: getBtcHistoricalPricesData(state),
    error: getBtcHistoricalPricesError(state)
  }), {
    fetchBtcPrices
  }
)(fitWidth(HistoricalBtcPrices))
