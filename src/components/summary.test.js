
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import { Summary } from './summary'
import { TAB } from '../static'

Enzyme.configure({ adapter: new Adapter() })

describe('Summary', () => {
  const mockSelectTab = jest.fn()

  afterEach(() => {
    mockSelectTab.mockClear()
  })

  it('should render correctly', () => {
    const wrapper = shallow(<Summary selected={TAB.PRICES} selectTab={mockSelectTab} />)

    expect(wrapper).toMatchSnapshot()
  })

  it('should highlight the selected tab"', () => {
    const wrapper = shallow(<Summary selected={TAB.PRICES} selectTab={mockSelectTab} />)
    const wrapperTicker = shallow(<Summary selected={TAB.TICKER} selectTab={mockSelectTab} />)

    expect(wrapper.find('.selected').contains('Prices')).toBe(true)
    expect(wrapperTicker.find('.selected').contains('Ticker')).toBe(true)
  })

  it('should dispatch an action when a tab is clicked"', () => {
    const wrapper = shallow(<Summary selected={TAB.PRICES} selectTab={mockSelectTab} />)

    expect(mockSelectTab).not.toHaveBeenCalled()

    wrapper.find('.selected').simulate('click', new Event('click'))

    expect(mockSelectTab).toHaveBeenCalledTimes(1)
    expect(mockSelectTab).toHaveBeenCalledWith(TAB.PRICES)
  })
})
