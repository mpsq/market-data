
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import React from 'react'
import { HistoricalBtcPrices } from './historical-btc-prices'

Enzyme.configure({ adapter: new Adapter() })

describe('HistoricalBtcPrices', () => {
  const className = 'my-class'
  const data = [{ x: new Date('2018-01-01'), y: 1000 }]
  const mockFetchBtcPrices = jest.fn()

  let defaultWrapper

  beforeEach(() => {
    defaultWrapper = shallow(<HistoricalBtcPrices {...{
      error: false,
      data,
      fetchBtcPrices: mockFetchBtcPrices,
      width: 400,
      className
    }} />)

    mockFetchBtcPrices.mockClear()
  })

  it('should render correctly', () => {
    expect(defaultWrapper).toMatchSnapshot()
  })

  it('should dispatch a fetchBtcPrices action"', () => {
    expect(mockFetchBtcPrices).not.toHaveBeenCalled()

    shallow(<HistoricalBtcPrices {...{
      error: false,
      data,
      fetchBtcPrices: mockFetchBtcPrices,
      width: 400,
      className
    }} />)

    expect(mockFetchBtcPrices).toHaveBeenCalledTimes(1)
  })

  it('should show a message when loading the data"', () => {
    const wrapperLoading = shallow(<HistoricalBtcPrices {...{
      error: false,
      data: [],
      fetchBtcPrices: mockFetchBtcPrices,
      width: 400,
      className
    }} />)
    const wrapperOk = defaultWrapper

    expect(wrapperLoading.find('.loading')).toHaveLength(1)
    expect(wrapperOk.find('.loading')).toHaveLength(0)
  })

  it('should show a message when there is an error"', () => {
    const wrapperError = shallow(<HistoricalBtcPrices {...{
      error: true,
      data,
      fetchBtcPrices: mockFetchBtcPrices,
      width: 400,
      className
    }} />)
    const wrapperOk = defaultWrapper

    expect(wrapperError.find('.error')).toHaveLength(1)
    expect(wrapperOk.find('.error')).toHaveLength(0)
  })
})
