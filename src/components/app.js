import _ from 'lodash/fp'
import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'
import { getConnectionStatus, getSelectedTab } from '../reducers'
import { TAB } from '../static'
import HistoricalBtcPrices from './historical-btc-prices'
import Summary from './summary'
import Table from './table'
import Ticker from './ticker'

import styles from './app.module.css'

const tabComponet = {
  [TAB.PRICES]: Table,
  [TAB.TICKER]: Ticker,
  [TAB.HISTORICAL]: HistoricalBtcPrices
}

export const App = props => {
  const connectionStatus = props.connectionStatus.toLowerCase()

  const Tab = tabComponet[props.selected]

  return (
    <div className={styles.root}>

      <header className={styles.header}>
        <div className={styles.hLabel}>
          <span className={styles.brand}>Blockchain Market Data</span>

          <div className={styles.connection}>
            <span className={styles[connectionStatus]}/> {_.startCase(connectionStatus)}
          </div>
        </div>
      </header>

      <Summary />

      <div className={styles.main}>
        <Tab className={styles.content}/>
      </div>

      <footer className={styles.footer}>
        Real-time data from:&nbsp;
        <a href="https://www.cryptocompare.com">cryptocompare.com</a>
      </footer>
    </div>
  )
}

App.propTypes = {
  connectionStatus: PropTypes.string.isRequired,
  selected: PropTypes.string.isRequired
}

export default connect(
  state => ({
    connectionStatus: getConnectionStatus(state),
    selected: getSelectedTab(state)
  })
)(App)
