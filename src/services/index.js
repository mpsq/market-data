import { format, subYears } from 'date-fns'
import _ from 'lodash/fp'
import 'rxjs/add/observable/dom/ajax'
import { Observable } from 'rxjs/Observable'
import { HIST_MARKET_PRICE_URL } from '../static'
import { getStream, status$ } from '../utils'

const map = _.map.convert({ cap: false })
const reduce = _.reduce.convert({ cap: false })
const dayFormat = 'YYYY-MM-DD'

const parseParams = reduce((acc, param, key) => `${acc}${acc.length === 1 ? '' : '&'}${key}=${param}`, '?')

const btcHistPricesParams = now => ({
  end: format(now, dayFormat),
  start: format(subYears(now, 1), dayFormat)
})

export const api = {
  getConnectionStatus: () => status$,

  getBtcHistoricalPrices: () => Observable
    .ajax({
      url: `${HIST_MARKET_PRICE_URL}${[parseParams(btcHistPricesParams(new Date()))]}`,
      crossDomain: true
    })
    .pluck('response')
    .map(({ bpi }) => map((value, key) => ({x: new Date(key), y: value}), bpi)),

  getTicker: subscriptions => getStream(subscriptions)
}
