export const FETCH_BTC_PRICES = 'FETCH_BTC_PRICES'
export const RECEIVE_BTC_PRICES = 'RECEIVE_BTC_PRICES'

export const GET_CONNECTION_STATUS = 'GET_CONNECTION_STATUS'
export const RECEIVE_CONNECTION_STATUS = 'RECEIVE_CONNECTION_STATUS'

export const GET_TICKER = 'GET_TICKER'
export const RECEIVE_BCH = 'RECEIVE_BCH'
export const RECEIVE_BTC = 'RECEIVE_BTC'
export const RECEIVE_ETH = 'RECEIVE_ETH'
export const RECEIVE_XRP = 'RECEIVE_XRP'
export const SELECT_TAB = 'SELECT_TAB'

export const fetchBtcPrices = () => ({
  type: FETCH_BTC_PRICES
})

export const receiveBtcPrices = response => ({
  data: response.data,
  error: response.error,
  type: RECEIVE_BTC_PRICES
})

export const selectTab = tab => ({
  tab,
  type: SELECT_TAB
})

export const receiveBCH = response => ({
  data: response.data,
  error: response.error,
  type: RECEIVE_BCH
})

export const receiveBTC = response => ({
  data: response.data,
  error: response.error,
  type: RECEIVE_BTC
})

export const receiveETH = response => ({
  data: response.data,
  error: response.error,
  type: RECEIVE_ETH
})

export const receiveXRP = response => ({
  data: response.data,
  error: response.error,
  type: RECEIVE_XRP
})

export const getConnectionStatus = () => ({
  type: GET_CONNECTION_STATUS
})

export const getTicker = () => ({
  type: GET_TICKER
})

export const receiveConnectionStatus = status => ({
  status,
  type: RECEIVE_CONNECTION_STATUS
})
