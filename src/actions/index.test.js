import * as Actions from './index'

describe('Actions', () => {
  it('should create an action to fetch bitcoin prices', () => {
    const expectedAction = {
      type: Actions.FETCH_BTC_PRICES
    }
    expect(Actions.fetchBtcPrices()).toEqual(expectedAction)
  })

  it('should create an action to receive bitcoin prices', () => {
    const data = [{}]
    const expectedAction = {
      data,
      error: false,
      type: Actions.RECEIVE_BTC_PRICES
    }
    expect(Actions.receiveBtcPrices({data, error: false})).toEqual(expectedAction)
  })

  it('should create an action to select at tab', () => {
    const tab = 'One'
    const expectedAction = {
      tab,
      type: Actions.SELECT_TAB
    }
    expect(Actions.selectTab(tab)).toEqual(expectedAction)
  })

  it('should create an action to receive BCH data', () => {
    const data = 'BCH'
    const expectedAction = {
      data,
      error: false,
      type: Actions.RECEIVE_BCH
    }
    expect(Actions.receiveBCH({data, error: false})).toEqual(expectedAction)
  })

  it('should create an action to receive BTC data', () => {
    const data = 'BTC'
    const expectedAction = {
      data,
      error: false,
      type: Actions.RECEIVE_BTC
    }
    expect(Actions.receiveBTC({data, error: false})).toEqual(expectedAction)
  })

  it('should create an action to receive ETH data', () => {
    const data = 'ETH'
    const expectedAction = {
      data,
      error: false,
      type: Actions.RECEIVE_ETH
    }
    expect(Actions.receiveETH({data, error: false})).toEqual(expectedAction)
  })

  it('should create an action to receive XRP data', () => {
    const data = 'XRP'
    const expectedAction = {
      data,
      error: false,
      type: Actions.RECEIVE_XRP
    }
    expect(Actions.receiveXRP({data, error: false})).toEqual(expectedAction)
  })

  it('should create an action to get the connection status', () => {
    const expectedAction = {
      type: Actions.GET_CONNECTION_STATUS
    }
    expect(Actions.getConnectionStatus('test')).toEqual(expectedAction)
  })

  it('should create an action to get ticker data', () => {
    const expectedAction = {
      type: Actions.GET_TICKER
    }
    expect(Actions.getTicker('test')).toEqual(expectedAction)
  })

  it('should create an action to receive the connection status', () => {
    const status = 'OK'
    const expectedAction = {
      type: Actions.RECEIVE_CONNECTION_STATUS,
      status
    }
    expect(Actions.receiveConnectionStatus(status)).toEqual(expectedAction)
  })
})
