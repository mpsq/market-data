import { RECEIVE_BTC_PRICES } from '../actions'

export default (state = { data: [], error: false }, action) => {
  if (action.type === RECEIVE_BTC_PRICES) {
    return {
      data: action.data,
      error: action.error
    }
  }

  return state
}
