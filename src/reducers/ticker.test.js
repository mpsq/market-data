import * as Actions from '../actions'
import reducer from './ticker'

describe('ticker reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({data: {}, error: false})
  })

  it('should handle RECEIVE_BCH', () => {
    const action = {
      data: { test: 1, openDailyHour: 10, price: 5 },
      error: false,
      type: Actions.RECEIVE_BCH
    }
    expect(reducer(undefined, action)).toEqual({
      data: {
        bch: {
          ...action.data,
          priceDailyDiff: -50
        }
      },
      error: false
    })
  })

  it('should handle RECEIVE_BTC', () => {
    const action = {
      data: { test: 1, openDailyHour: 10, price: 5 },
      error: false,
      type: Actions.RECEIVE_BTC
    }
    expect(reducer(undefined, action)).toEqual({
      data: {
        btc: {
          ...action.data,
          priceDailyDiff: -50
        }
      },
      error: false
    })
  })

  it('should handle RECEIVE_ETH', () => {
    const action = {
      data: { test: 1, openDailyHour: 10, price: 5 },
      error: false,
      type: Actions.RECEIVE_ETH
    }
    expect(reducer(undefined, action)).toEqual({
      data: {
        eth: {
          ...action.data,
          priceDailyDiff: -50
        }
      },
      error: false
    })
  })

  it('should handle RECEIVE_XRP', () => {
    const action = {
      data: { test: 1, openDailyHour: 10, price: 5 },
      error: false,
      type: Actions.RECEIVE_XRP
    }
    expect(reducer(undefined, action)).toEqual({
      data: {
        xrp: {
          ...action.data,
          priceDailyDiff: -50
        }
      },
      error: false
    })
  })
})
