import { RECEIVE_CONNECTION_STATUS } from '../actions'
import { CONNECTION_STATUS } from '../static'

export default (state = CONNECTION_STATUS.OFFLINE, action) => {
  if (action.type === RECEIVE_CONNECTION_STATUS) {
    return action.status
  }

  return state
}
