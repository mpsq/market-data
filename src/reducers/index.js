import { combineReducers } from 'redux'
import connectionStatus from './connection-status'
import historicalBtcPrices from './historical-btc-prices'
import selectedTab from './selected-tab'
import ticker from './ticker'

export const getConnectionStatus = state => state.connectionStatus
export const getBtcHistoricalPricesData = state => state.historicalBtcPrices.data
export const getBtcHistoricalPricesError = state => state.historicalBtcPrices.error
export const getSelectedTab = state => state.selectedTab
export const getTickerData = state => state.ticker.data
export const getTickerError = state => state.ticker.error

export const rootReducer = combineReducers({
  connectionStatus,
  historicalBtcPrices,
  selectedTab,
  ticker
})
