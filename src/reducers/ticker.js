import _ from 'lodash/fp'
import { RECEIVE_BCH, RECEIVE_BTC, RECEIVE_ETH, RECEIVE_XRP } from '../actions'

const receiveActions = [RECEIVE_BCH, RECEIVE_BTC, RECEIVE_ETH, RECEIVE_XRP]

export default (state = { data: {}, error: false }, action) => {
  if (receiveActions.includes(action.type) && action.error) {
    return {
      ...state,
      error: true
    }
  }

  if (action.type === RECEIVE_BCH) {
    const bch = _.merge(state.data.bch, action.data)

    return {
      data: {
        ...state.data,
        bch: {
          ...bch,
          priceDailyDiff: +(((bch.price - bch.openDailyHour) / bch.openDailyHour) * 100).toFixed(2)
        }
      },
      error: false
    }
  }

  if (action.type === RECEIVE_BTC) {
    const btc = _.merge(state.data.btc, action.data)

    return {
      data: {
        ...state.data,
        btc: {
          ...btc,
          priceDailyDiff: +(((btc.price - btc.openDailyHour) / btc.openDailyHour) * 100).toFixed(2)
        }
      },
      error: false
    }
  }

  if (action.type === RECEIVE_ETH) {
    const eth = _.merge(state.data.eth, action.data)

    return {
      data: {
        ...state.data,
        eth: {
          ...eth,
          priceDailyDiff: +(((eth.price - eth.openDailyHour) / eth.openDailyHour) * 100).toFixed(2)
        }
      },
      error: false
    }
  }

  if (action.type === RECEIVE_XRP) {
    const xrp = _.merge(state.data.xrp, action.data)

    return {
      data: {
        ...state.data,
        xrp: {
          ...xrp,
          priceDailyDiff: +(((xrp.price - xrp.openDailyHour) / xrp.openDailyHour) * 100).toFixed(2)
        }
      },
      error: false
    }
  }

  return state
}
