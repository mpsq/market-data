import { SELECT_TAB } from '../actions'
import { TAB } from '../static'

export default (state = TAB.TICKER, action) => {
  if (action.type === SELECT_TAB) {
    return action.tab
  }

  return state
}
