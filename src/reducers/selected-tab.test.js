import * as Actions from '../actions'
import { TAB } from '../static'
import reducer from './selected-tab'

describe('selectedTab reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(TAB.TICKER)
  })

  it('should handle SELECT_TAB', () => {
    const action = {
      type: Actions.SELECT_TAB,
      tab: 'new'
    }
    expect(reducer(null, action)).toEqual('new')
  })
})
