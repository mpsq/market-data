import * as Actions from '../actions'
import reducer from './historical-btc-prices'

describe('historicalBtcPrices reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({'data': [], 'error': false})
  })

  it('should handle RECEIVE_BTC_PRICES', () => {
    const action = {
      type: Actions.RECEIVE_BTC_PRICES,
      data: [1],
      error: false
    }
    expect(reducer(undefined, action)).toEqual({
      data: [1],
      error: false
    })
  })

  it('should handle errors on RECEIVE_BTC_PRICES', () => {
    const action = {
      type: Actions.RECEIVE_BTC_PRICES,
      data: [],
      error: true
    }
    expect(reducer(undefined, action)).toEqual({
      data: [],
      error: true
    })
  })
})
