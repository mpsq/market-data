import * as Actions from '../actions'
import { CONNECTION_STATUS } from '../static'
import reducer from './connection-status'

describe('connectionStatus reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toBe(CONNECTION_STATUS.OFFLINE)
  })

  it('should handle RECEIVE_CONNECTION_STATUS', () => {
    const action = {
      type: Actions.RECEIVE_CONNECTION_STATUS,
      status: CONNECTION_STATUS.ONLINE
    }
    expect(reducer(null, action)).toBe(CONNECTION_STATUS.ONLINE)
  })
})
