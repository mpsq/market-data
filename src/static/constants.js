export const CRYPTOCOMPARE_COMMAND = {
  ADD: 'SubAdd',
  REMOVE: 'SubRemove'
}

export const CRYPTOCOMPARE_OPT = { path: '/socket.io' }

export const CRYPTOCOMPARE_URI = 'https://streamer.cryptocompare.com'

export const CONNECTION_STATUS = {
  ERROR: 'ERROR',
  OFFLINE: 'OFFLINE',
  ONLINE: 'ONLINE'
}

export const CURRENCY = [
  'BCH',
  'BTC',
  'ETH',
  'XRP'
]

export const PRICE_FLAG = {
  1: 'UP',
  2: 'DOWN',
  4: 'UNCHANGED'
}

export const TAB = {
  HISTORICAL: 'historical',
  PRICES: 'prices',
  TICKER: 'ticker'
}

export const HIST_MARKET_PRICE_URL = 'https://api.coindesk.com/v1/bpi/historical/close.json'
