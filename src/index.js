import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { createEpicMiddleware } from 'redux-observable'
import * as actions from './actions'
import App from './components/app'
import { rootEpic } from './epics'
import { rootReducer } from './reducers'

import styles from './index.module.css'

const epicMiddleware = createEpicMiddleware(rootEpic)

const store = createStore(
  rootReducer,
  applyMiddleware(epicMiddleware)
)

store.dispatch(actions.getConnectionStatus())
store.dispatch(actions.getTicker())

const rootNode = document.getElementById('root')
rootNode.classList.add(styles.root)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootNode
)
