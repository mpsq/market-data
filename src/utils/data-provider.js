import io from 'socket.io-client'
import Rx from 'rxjs/Rx'
import { CONNECTION_STATUS, CRYPTOCOMPARE_COMMAND, CRYPTOCOMPARE_URI, CRYPTOCOMPARE_OPT } from '../static'
import CCC from './ccc-streamer-utilities'

let socket

const statusSub = new Rx.BehaviorSubject(CONNECTION_STATUS.OFFLINE)

const connect = () => {
  socket = io(CRYPTOCOMPARE_URI, CRYPTOCOMPARE_OPT)

  socket.on('m', () => { statusSub.next(CONNECTION_STATUS.ONLINE) })
  socket.on('error', () => { statusSub.next(CONNECTION_STATUS.ERROR) })
}

export const getStream = subs => {
  if (!socket) {
    connect()
  }

  const stream$ = Rx.Observable.create(observer => {
    socket.on('m', message => {
      const messageType = message.substring(0, message.indexOf('~'))

      if (messageType === CCC.STATIC.TYPE.CURRENTAGG) {
        observer.next(CCC.CURRENT.unpack(message))
      }
    })

    socket.on('error', error => {
      observer.error(error)
    })

    return () => socket.emit(CRYPTOCOMPARE_COMMAND.REMOVE, { subs })
  })

  socket.emit(CRYPTOCOMPARE_COMMAND.ADD, { subs })

  return stream$
    .shareReplay(1)
}

export const status$ = statusSub
  .distinctUntilChanged()

export const disconnect = () => {
  socket.close()
  socket = null
}
