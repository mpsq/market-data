import _ from 'lodash/fp'

const locale = new Intl.NumberFormat('en-EN')

export default number => {
  if (!_.isNil(number) && Number.isFinite(number)) {
    return locale.format(number.toFixed(2))
  }

  return undefined
}
