
export default raw => {
  const data = raw || {}

  return {
    fromSymbol: data.FROMSYMBOL,
    toSymbol: data.TOSYMBOL,
    priceFlag: data.FLAGS,
    lastUpdate: data.LASTUPDATE ? data.LASTUPDATE * 1000 : undefined,

    price: data.PRICE,

    lastVolume: data.LASTVOLUME,
    lastVolumeTo: data.LASTVOLUMETO,

    volumeDailyHour: data.VOLUME24HOUR,
    volumeDailyHourTo: data.VOLUME24HOURTO,
    volumeHour: data.VOLUMEHOUR,
    volumeHourTo: data.VOLUMEHOURTO,

    highDailyHour: data.HIGH24HOUR,
    lowDailyHour: data.LOW24HOUR,
    openDailyHour: data.OPEN24HOUR
  }
}
