import TickerVO from './ticker-vo'

describe('TickerVO util', () => {
  it('should return the transformed data', () => {
    const update = Date.now()
    expect(TickerVO({
      FROMSYMBOL: 1000,
      TOSYMBOL: 1000,
      FLAGS: 1000,
      LASTUPDATE: update,
      PRICE: 1000,
      LASTVOLUME: 1000,
      LASTVOLUMETO: 1000,
      VOLUME24HOUR: 1000,
      VOLUME24HOURTO: 1000,
      VOLUMEHOUR: 1000,
      VOLUMEHOURTO: 1000,
      HIGH24HOUR: 1000,
      LOW24HOUR: 1000,
      OPEN24HOUR: 1000
    })).toEqual({
      'fromSymbol': 1000,
      'highDailyHour': 1000,
      'lastUpdate': update * 1000,
      'lastVolume': 1000,
      'lastVolumeTo': 1000,
      'lowDailyHour': 1000,
      'openDailyHour': 1000,
      'price': 1000,
      'priceFlag': 1000,
      'toSymbol': 1000,
      'volumeDailyHour': 1000,
      'volumeDailyHourTo': 1000,
      'volumeHour': 1000,
      'volumeHourTo': 1000
    })
  })

  it('should handle falsy data', () => {
    expect(TickerVO(null)).toEqual({})
    expect(TickerVO(undefined)).toEqual({})
    expect(TickerVO({})).toEqual({})
  })
})
