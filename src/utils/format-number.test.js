import FormatNumber from './format-number'

describe('FormatNumber util', () => {
  it('should return the transformed data', () => {
    expect(FormatNumber(100000)).toEqual('100,000')
  })

  it('should handle falsy data', () => {
    expect(FormatNumber(null)).toEqual(undefined)
    expect(FormatNumber(undefined)).toEqual(undefined)
    expect(FormatNumber(Number.POSITIVE_INFINITE)).toEqual(undefined)
  })
})
