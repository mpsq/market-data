# Installation
`npm i`

# Running the dev server
`npm start`

The app should be running on [localhost:3000](http://localhost:3000).

# Running the tests
`npm test`

## Notes
yarn can be used instead of npm

## TODO

- [ ] Finish writting tests (services, epic and data-provider files)
- [X] Add 24H % change in ticker and prices
- [X] Implement .catch() in ajax rxjs streams to handle errors gracefully
- [ ] Improve WebSocket implementation (reconnection, connection lost,...)
- [ ] Implement a responsive version
- [ ] Convert styles to cssnext (or SASS)

(sorted by priority)
